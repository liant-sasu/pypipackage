# Copyleft 2022 Liant SASU
#
# MIT Licensed
#
# Owner: Liant SASU / French company registered SIRET 90897689700011
# Author: Roland Laurès <roland@liant.dev>

def test_main(capsys):
    from pypipackage.__main__ import main
    main()
    captured = capsys.readouterr()
    assert captured.out == "Hello world from Liant and Roland.\n"