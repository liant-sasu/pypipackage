# pypi Package Template
A template for an application Hello World that build as a pypi package, push on Gitlab package repository and Pypi.

## Getting started
This is a simple Helloworld program, easy to use to create a new pypi package from.

It followed the steps proposed [here](https://packaging.python.org/en/latest/tutorials/packaging-projects/).

## Starting from that template

### As a template from gitlab (future)
Once it is integrated in Gitlab, it will be available in the list of project template [here](https://gitlab.com/projects/new#create_from_template) (don't use this URL, since it is probably not what you want).

### By fork
Just fork the project, and edit as you like.
Be aware that just after the fork, it won't work properly since you cannot push to my pypi account.
Follow the steps in the next section.

## Post fork/creation setup
The project need some setting to be working. Here are all the steps you must take.

TODO: create the steps and describe.

## Support
You can ask questions through the [issues](https://gitlab.com/liant-sasu/pypipackage/-/issues) or by contacting me (see the authors section below).

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
Contributions are welcome and can be made through creating an issue, a PR...
Just keep it professional and limited to the project goal stated in the project description.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
This project is licensed under the MIT license.

Once forked and you have made sufficient efforts to change the programm, then you can change it.

If some files remain mainly unchanged they must keep a reference to the original project and license.

## Project status
This project is in early development stage.
