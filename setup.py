# Copyleft 2022 Liant SASU
#
# MIT Licensed
#
# Owner: Liant SASU / French company registered SIRET 90897689700011
# Author: Roland Laurès <roland@liant.dev>
import sys
sys.path.append('src')

from setuptools import setup, find_packages

from pypipackage import __last_version__

setup(
    version = __last_version__,
    packages=find_packages(
        where='src',
        include=['pypipackage*'],  # ["*"] by default
    ),
)
