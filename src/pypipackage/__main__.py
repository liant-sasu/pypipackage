# Copyleft 2022 Liant SASU
#
# MIT Licensed
#
# Owner: Liant SASU / French company registered SIRET 90897689700011
# Author: Roland Laurès <roland@liant.dev>

from .welcome_message import message

def main():
  print(message)

if __name__ == "__main__":
    main()