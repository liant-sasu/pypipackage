# Copyleft 2022 Liant SASU
#
# MIT Licensed
#
# Owner: Liant SASU / French company registered SIRET 90897689700011
# Author: Roland Laurès <roland@liant.dev>

# Update this version before you tagged your code (obviously).
# With the pipeline initially created, this version will only serve as a
# base to increment local version or developpement ones.
__last_version__ = '0.0.1'